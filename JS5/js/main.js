
const tabs = document.querySelector('#tabs');
const tabcontent = document.querySelector('#contentoftab');


tabs.onclick = function(event) {
  tabs.querySelector(".active").classList.remove("active");
  tabcontent.querySelector("li:not([hidden])").hidden = true;
  
  event.target.classList.add("active");
  tabcontent.children[event.target.dataset.index].hidden = false;
  
}
for (let i = 0; i < tabs.children.length; i++){
    tabs.children[i].dataset.index = i;
    tabcontent.children[i].dataset.index=i;
}
for (let i = 0; i < tabcontent.children.length; i++){
    tabs.children[i].dataset.index = i;

    if(i)
      tabcontent.children[i].hidden = true;
  }

s