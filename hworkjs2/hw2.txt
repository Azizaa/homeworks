1.We can simply define a function as a subprogram inside the main program that performs a 
specific task . Function is re-usable , organized code . If we call a function the program 
leaves current section of code and start to execute first line inside the function .
It starts execution from to to bottom , after when it ends execution inside of a function 
it returns to the part of the code where it was before .

2.An argument is actual value passed to the function. The argument object contains an 
array of the arguments that are used when the function is called .The function only
 gets to know the values, but not location of argument.
When function changes an argument's value, it does not change original value of
 the parameter's.

